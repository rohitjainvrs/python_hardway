def last_digit(n1, n2):
    a=n1%10
    b=0
    if n2==0:
        return 1
    elif a == 0:
        return 0
    elif a == 1 or a==5 or a == 6:
        return a
    elif a==2:
        b=n2%4
        if b==0:
            return 6
        elif b==1:
            return 2
        elif b==2:
            return 4
        elif b==3:
            return 8
    elif a==3:
        b=n2%4
        if b==0:
            return 1
        elif b==1:
            return 3
        elif b==2:
            return 9
        elif b==3:
            return 7
    elif a == 4:
        b=n2%2
        if b==0:
            return 6
        else:
            return 4
    elif a==7:
        b=n2%4
        if b==0:
            return 1
        elif b==1:
            return 7
        elif b==2:
            return 9
        elif b==3:
            return 3
    elif a==8:
        b=n2%4
        if b==0:
            return 6
        elif b==1:
            return 8
        elif b==2:
            return 4
        elif b==3:
            return 2
    elif a == 9:
        b=n2%2
        if b==0:
            return 1
        else:
            return 9
