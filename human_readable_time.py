def make_readable(seconds):
    hh=0
    mm=0
    ss=seconds
    while ss>=0:
        ss-=60
        if ss<=0:
            break
        else:
            mm+=1
            while mm>60:
                mm-=60
                if mm<=0:
                    mm+60
                    break
                else:
                    hh+=1
    if ss<0:
        ss+=60
    print("%02d"%hh+":"+"%02d"%mm+":"+"%02d"%ss)
make_readable(86399)
